var pushNotification;

var hindvaApp=angular.module('hindvaApp', [ "ngRoute","ngStorage","mobile-angular-ui","ui.bootstrap"]);
/* to display special characters*/
hindvaApp.filter('html',function($sce){
    return function(input){
        return $sce.trustAsHtml(input);
    }
})

function navigateToStore()
{
   window.open("https://play.google.com/store/apps/details?id=com.easternts.hindva", "_system");
}

function removeHTMLTags(){
		var strInputCode = document.getElementById("overviewid").innerHTML;
		strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1){
			return (p1 == "lt")? "<" : ">";
		});
		var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
		return strTagStrippedText;
}
function openlinked()
{
	window.open("http://in.linkedin.com/in/keyurkheni", "_system");
}

function opentwitter()
{
	window.open("http://www.twitter.com/kkheni", "_system");	
}
function openyoutube()
{
	window.open("http://www.youtube.com/user/Hindva", "_system");	
	
}
function openfacebook()
{
	window.open("http://www.facebook.com/Hindva", "_system");	
}
function openhindva()
{
	window.open("http://www.hindva.com", "_system");	
}

/* push notification functions*/
function onNotificationGCM(e) {
	switch( e.event )
	{
		case 'registered':
			if ( e.regid.length > 0 )
			{
				var gcmid = e.regid;
				var s = document.getElementById('gcmid');
            		s.value = gcmid;		
			}
	     break;
	     
	     case 'message':
	     	// if this flag is set, this notification happened while we were in the foreground.
	     	// you might want to play a sound to get the user's attention, throw up a dialog, etc.
	     	if (e.foreground)
	     	{
				
			}
			else
			{	
			
			}
				
	     break;
	     case 'error':
			
	     break;
	     
	     default:		
	     break;
	 }
}
function tokenHandler (result) {
}
			
function successHandler (result) {
	
}
function errorHandler (error) {
	
}

function onDeviceReady() {
	//analytics.startTrackerWithId('UA-50746286-3');
	 
	if(localStorage.getItem('registerid') == 0 || localStorage.getItem("registerid") === null)
	{
		pushNotification = window.plugins.pushNotification;
 		if (device.platform == 'android' || device.platform == 'Android')
 		{
 			
			pushNotification.register(successHandler, errorHandler, {"senderID":"637851886767","ecb":"onNotificationGCM"});					
		}
	}
	else 
	{
		
	}	
}
document.addEventListener('deviceready', onDeviceReady, true);
/* end of push notification functions*/



hindvaApp.config(function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/home', {
                templateUrl : 'partials/home.html',
                controller  : 'mainController',
                label: 'Home'
            })
            .when('/success-story', {
                templateUrl : 'partials/success-story.html',
                controller  : 'success-storyController'
            })
            .when('/news', {
                templateUrl : 'partials/news.html',
                controller  : 'newsController'
            })
            .when('/contact-us', {
                templateUrl : 'partials/contact-us.html',
                controller  : 'contactController',
                label: 'Contact Us'
            })
            .when('/opportunity', {
                templateUrl : 'partials/opportunity.html',
                controller  : 'opportunityController',
                label: 'Opportunities'
            })
            .when('/about', {
                templateUrl : 'partials/about.html',
                controller  : 'aboutController'                  
            })
            .when('/subscribe', {
                templateUrl : 'partials/subscribe.html',
                controller  : 'subscribeController',
            })
            .when('/ProjectSelect/', {
                templateUrl : 'partials/allprojects.html',
                controller  : 'allprojectController',
                label: 'Projects'
            })
            .when('/ProjectSelect/:projectId', {
                templateUrl : 'partials/projects.html',
                controller  : 'projectController',
                label: 'Projects'
            })
            .when('/Project/:projectId', {
                templateUrl : 'partials/project-detail.html',
                controller  : 'projectDetailController',
                label: 'Projects'
            })
            .when('/Project-Option', {
                templateUrl : 'partials/project-options.html',
                controller  : 'projectoptionController',
                label: 'Projects'
            })
            .when('/Search-Project/:typeID/:status', {
                templateUrl : 'partials/search-project.html',
                controller  : 'searchProjectController',
                label: 'Projects'
            })
            .when('/gallery', {
                templateUrl : 'partials/gallery.html',
                controller  : 'galleryController',
                label: 'Gallery'
            })
             .when('/opportunity', {
                templateUrl : 'partials/opportunity1.html',
                controller  : 'opportunityController',
                label: 'Opportunities'
            })
            .when('/notification', {
                templateUrl : 'partials/notification.html',
                controller  : 'notificationController',
                label: 'Notifications'
            })
            .when('/reach-us', {
                templateUrl : 'partials/reachus.html',
                controller  : 'reachusController',
                label: 'Reach Us'
            })
            .otherwise({
            	  templateUrl : 'partials/subscribe.html',
                controller  : 'subscribeController',

      		});           
});

/*
var ref = '';
function changeBackgroundColor() {
        ref.insertCSS({
            code: ".sh{ display : inline-block;}"
         }, 
        function() {
            alert("Styles Altered");
        })
        
}
*/

/* called on every route*/
hindvaApp.run(function ($rootScope,$localStorage,$http,$location) {
	
    $rootScope.homefooter = false; 
    $rootScope.innerfooter = false; 
    $rootScope.subscribeheader = false;
   
    $rootScope.$storage = $localStorage.$default({
          totalcnt: 0,
          oldcnt: 0 ,
          finalcnt : 0,
          subscribe : 0
    });
    /* downloading data by open file native plugin*/
     $rootScope.downloaddata = function(url) 
 	{
 		window.openFileNative.open(url); 
 		
     };
     
     $rootScope.opengallery = function(imgid) 
 	{
		
 		var fpath = 'http://www.hindva.com/photogallery1.php?imgid=' + imgid;
 		
 		var ref = window.open(fpath, '_blank', 'location=no', 'EnableViewPortScale=yes');	
 		ref.addEventListener('loadstart', function(event) 
 		{ 
			navigator.notification.activityStart("Loading", "Please wait for a while."); 
		});
        ref.addEventListener('loadstop', function(event) { navigator.notification.activityStop(); 
		         	
         	});

     };
    
    $rootScope.floorplangallery = function(projectid,planid) 
 	{
 		var fpath = 'http://www.hindva.com/floorplangallery.php?projectid=' + projectid + '&floorid=' + planid;
 		var ref1 = window.open(fpath, '_blank', 'location=no');	
 		ref1.addEventListener('loadstart', function(event) 
 		{ navigator.notification.activityStart("Loading", "Please wait for a while."); }
 		);
         	ref1.addEventListener('loadstop', function(event) { navigator.notification.activityStop(); 
		         	
         	});
     };
     
     $rootScope.progallery = function(projectid,type) 
 	{
		var filename = '';
		
		if(type == 'photo')
		{
			filename = 'projectgallery.php';
		}
		else
		{
			filename = 'sitegallery.php';
		}
		
 		var fpath = 'http://www.hindva.com/'+ filename +'?projectid=' + projectid;
 		
 		var ref = window.open(fpath, '_blank', 'location=no');	
 		ref.addEventListener('loadstart', function(event) 
 		{ navigator.notification.activityStart("Loading", "Please wait for a while."); }
 		);
         	ref.addEventListener('loadstop', function(event) { 
         	navigator.notification.activityStop();     	
         	 });
         	 
     };
     
    /* change header and footer */
    $rootScope.changeview = function() 
 	{
 		$rootScope.subscribeheader 	= false;
		$rootScope.homefooter 		= false;
		$rootScope.innerfooter 		= true;
		$rootScope.subfooter 		= false;	    	
     };
     
     /* check for internet connection*/
	$rootScope.checkConnection = function () {
		/* if not connected return true*/
		if (navigator.network.connection.type == Connection.NONE) {
			navigator.notification.alert('Please check your Internet Connection', function () {}, "Warning", "");
			$location.path('/home');
		} else {

		}
	};
    
});

/* serailiaze data for sending to server */
function serialize(obj, prefix) {
    var str = [];
    for (var p in obj) {
      if (obj.hasOwnProperty(p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
              v = obj[p];
          str.push(typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}

function exapp()
{
	navigator.app.exitApp();
}

function onBackKeyDown()
{
    // Handle the back button
    navigator.app.exitApp();
    
}

hindvaApp.controller('mainController', function($scope,$rootScope,$location,$window) {

	$rootScope.defaultheader = true;
	$rootScope.subscribeheader 	= false;
	$rootScope.homefooter = true;
	$rootScope.innerfooter = false;
	$rootScope.subfooter = false;	
	
    $scope.imgarray = ["img/slider1.jpg","img/slider2.jpg","img/slider3.jpg","img/slider4.jpg"];
 	$scope.myInterval = 3000;
 	
 	document.addEventListener("backbutton", onBackKeyDown, false);

});

hindvaApp.controller('aboutController', function($scope,$http,$window,$rootScope,$location) {	
		
		$rootScope.changeview();
		$rootScope.checkConnection();
    	$scope.title = 'About Us';
    	$scope.about = null;
    	$scope.loader = true;
		
  		$http.get('http://www.hindva.com/data/about.php')
         	.success(function (data) {
            	$scope.about = data;
            	 $scope.items = [
                    {
                        name: "item1",
                        desc: "About Us",
                        information : $scope.about.content
                    },
                    {
                        name: "item2",
                        desc: "Our Values",
                        information : $scope.about.values 
                    },
                    {
                        name: "item3",
                        desc: "Health & Safety",
                        information : $scope.about.health
                    },
                    {
                        name: "item4",
                        desc: "Environment",
                        information : $scope.about.env
                    }
                ];
			
			$scope.default = $scope.items[1];
			$scope.$parent.isopen = ($scope.$parent.default === $scope.item);

                $scope.$watch('isopen', function (newvalue, oldvalue, $scope) {
                    $scope.$parent.isopen = newvalue;
                });
                $scope.loader = false;
         	})
         	.error(function (data, status, headers, config) {
         	});
         	
    		
      	$scope.backfun = function() {
    			$location.path('/home');
      	}
      	
      document.removeEventListener("backbutton", onBackKeyDown, false);
});

hindvaApp.controller('success-storyController', function($scope,$rootScope,$http,$window,$location) {
			$rootScope.checkConnection();
		  	$rootScope.changeview();
			$scope.title = 'Success Stories';
			$scope.storyList = null;
			$scope.loader = true;
			
			
			$http.get('http://www.hindva.com/data/success-story.php')
         		.success(function (data) {
            	 $scope.storyList = data;
            	 $scope.loader = false;
         		})
         		.error(function (data, status, headers, config) {
            
         		});
         		
         	 	$scope.backfun = function() {
      			$location.path('/home');
      		}
      	document.removeEventListener("backbutton", onBackKeyDown, false);
});

hindvaApp.controller('newsController', function($scope,$rootScope,$http,$window,$location) {
		$rootScope.checkConnection();
		$rootScope.changeview();		
		$scope.title = 'News & Events';
		$scope.newsList = null;
		$scope.loader = true;
	
		$http.get('http://www.hindva.com/data/news.php')
         		.success(function (data) {
             		$scope.newsList = data;
             		$scope.loader = false;
         		})
         		.error(function (data, status, headers, config) {
         	});
    
         	
        $scope.backfun = function() {
			$location.path('/home');
      }	
      document.removeEventListener("backbutton", onBackKeyDown, false);
});

hindvaApp.controller('notificationController', function($scope,$http,$window,$rootScope,$location) {
		$rootScope.changeview();
		$scope.title = 'Notifications';
		$scope.loader = true;
		
		setTimeout(function() {
		$rootScope.checkConnection();
 		 $scope.notificationList = null;
			var dataObject = {
          	enabled : '1'
       	};
      // serialize data before sending       
      	var serdata = serialize(dataObject);
		$http({
    	  	url: "http://www.hindva.com/data/notification.php",
       	method: "POST",
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
         data: serdata
    		}).success(function(data, status, headers, config) {
				$scope.notificationList = data;
				$scope.loader = false;
    		}).error(function(data, status, headers, config) {
     		
		});  
    			
  	}, 2000);	
    		
		
        $scope.backfun = function() {
        	$location.path('/home');
      } 
      document.removeEventListener("backbutton", onBackKeyDown, false);	
});
	
hindvaApp.controller('galleryController', function($scope,$http,$window,$rootScope,$location,$modal) {
	$rootScope.checkConnection();
	$rootScope.changeview();
  	$scope.loader = true;
  	$scope.imgappend = '';
    	if(window.innerWidth <= 480){
    		$scope.imgappend = "_mobile1.jpg";
    	}else {
    		$scope.imgappend = "";
    	}
	$scope.gallerytitle = 'Gallery';
	
    $scope.galleryList = null;
	$http.get('http://www.hindva.com/data/gallery.php')
     .success(function (data) {
     	$scope.galleryList = data;
     	$scope.loader = false;
     }).error(function (data, status, headers, config) {}); 
    
      $scope.backfun = function() {
      	$location.path('/home');
      }
      
    /* modal box */
	$scope.animationsEnabled = true;

	$scope.openimg = function (imgname,name) {
		
			var modalInstance = $modal.open({
			  animation: $scope.animationsEnabled,
			  templateUrl: 'imgModalContent.html',
			  controller: 'ImgModalInstanceCtrl',
			  size: 'lg',
			  resolve: {
			  imgsrc: function () {
				  return imgname;
			  },
			   imgnm: function () {
				  return name;
			  }
			}
			});
		
	};
	
  /* end of modal box */
      
	document.removeEventListener("backbutton", onBackKeyDown, false);
});

hindvaApp.controller('ImgModalInstanceCtrl', function ($scope, $modalInstance,$http,$rootScope,imgsrc,imgnm) 
{
	$scope.imgsrc = imgsrc;
	$scope.imgnm = imgnm;
    $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
    $rootScope.customername = '';
  };
  
});

hindvaApp.controller('contactController', function($scope,$http,$route,$window,$rootScope,$location) {
	$rootScope.checkConnection();
	$rootScope.changeview();
	$scope.title = 'Inquiry';
	
	
    	$scope.formInfo = {};
    	
    	$scope.nameerror = false;
    	$scope.subjecterror = false;
    	$scope.messageerror = false;
    	$scope.emailerror = false;
    	
    	$scope.errormsg= function () {
    	if($scope.contactform.name.$error.required)
    	{
    				$scope.nameerror = true;	
    	}
    	else
    	{
    				$scope.nameerror = false;	
    	}
	};
		
	$scope.submsg= function () {
    	if($scope.contactform.subject.$error.required)
    	{
    				$scope.subjecterror = true;	
    	}
    	else
    	{
    				$scope.subjecterror = false;	
    	}
	};
		
	$scope.messfun= function () {
    	if($scope.contactform.message.$error.required)
    	{
    				$scope.messageerror = true;	
    	}
    	else
    	{
    				$scope.messageerror = false;	
    	}
	};
	$scope.emailfun= function () {
    	if($scope.contactform.email.$invalid )
    	{
    				$scope.emailerror = true;	
    	}
    	else
    	{
    				$scope.emailerror = false;	
    	}
	};
 
     $scope.formInfo.submitTheForm = function(item, event) {
     var dataObject = {
          name : $scope.formInfo.name,
          email : $scope.formInfo.email,
          subject : $scope.formInfo.subject,
          message : $scope.formInfo.message
         
     };
       
     //serialize data before sending
     
    var serdata = serialize(dataObject);
      
	$http({
    	  url: "http://www.hindva.com/sendmail.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    	}).success(function(data, status, headers, config) {
    		navigator.notification.alert('Your inquiry has been send successfully.We will be in touch with you Soon.', function(){}, "Success", "");	
    		
			$route.reload();   	
    	}).error(function(data, status, headers, config) {
    			navigator.notification.alert('There occured some problem in submitting the details.Please try again', function(){}, "Failure", "");	
     		
		});	 	 
     }
      $scope.backfun = function() {
      		$location.path('/home');
      }
	document.removeEventListener("backbutton", onBackKeyDown, false);
});
hindvaApp.controller('subscribeController', function($scope,$http,$route,$window,$rootScope,$location) {
	
	
	if($rootScope.$storage.subscribe == 1)
	{
		$location.path('/home');
		
	}
	
		
	$rootScope.subscribeheader = true;
	$rootScope.subfooter = true;
	$rootScope.homefooter = false;
  	$rootScope.innerfooter = false;
	$scope.title = 'Subscribe';
	
	if($rootScope.$storage.subscribe == 0){
	$scope.loader = true;
	setTimeout(function() {
    	$scope.$apply(function() {
     			$scope.loader = false;
    			});
  	}, 2000);		
  	}
   	$scope.formInfo = {};

	$scope.nameerror = false;
    	$scope.mobileerror = false;
    	$scope.emailerror = false;
    	$scope.errormsg= function () {
    	if($scope.subscribeform.name.$error.required)
    	{
    				$scope.nameerror = true;	
    	}
    	else
    	{
    				$scope.nameerror = false;	
    	}
	};
		
	$scope.mobilemsg= function () {
    	if($scope.subscribeform.mobile.$error.required)
    	{
    			$scope.mobileerror = true;	
    	}
    	else
    	{
    			$scope.mobileerror = false;	
    	}
	};
		
	$scope.emailfun= function () {
    	if($scope.subscribeform.email.$invalid )
    	{
    				$scope.emailerror = true;	
    	}
    	else
    	{
    				$scope.emailerror = false;	
    	}
	};
     $scope.formInfo.submitTheForm = function(item, event) {
    
    
    	if(navigator.network.connection.type == Connection.NONE)
    	{
    			navigator.notification.alert('Please check your Internet Connection', function(){}, "Warning", "");
    			$location.path('/subscribe');
    	
    	}
    
	/* get gcm id and insert with form details*/
	
	   var gcmregid = document.getElementById("gcmid").value;
	   localStorage.setItem('registerid',gcmregid);
    		
    //   console.log("--> Submitting form");
       var dataObject = {
          subname : $scope.formInfo.name,
          subemail : $scope.formInfo.email,
          submobile : $scope.formInfo.mobile,
          gcmid : gcmregid
       };
      // serialize data before sending       
      var serdata = serialize(dataObject);
      console.log(serdata);
	  $http({
    	  url: "http://www.hindva.com/data/subscription.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    	}).success(function(data, status, headers, config) {
		if(data.msg == 1)
    		{
    			navigator.notification.alert('Your subscription has been done successfully...', function(){}, "Success", "");	
    		}
    		else
    		{
    			navigator.notification.alert('You are already subscribed to this application...', function(){}, "Already Applied", "");	
    		}  
    		
		 //alert("Storage:" + localStorage.getItem(registerid));		
    		 
    		 $rootScope.$storage.subscribe = 1;
		 $location.path("/home") ;
		
    		}).error(function(data, status, headers, config) {
    			
     	//navigator.notification.alert('There occured some problem submitting the form..Please Try Again', function(){}, "", "");
		});	
		 	
     }
     
});
hindvaApp.controller('allprojectController', function($scope,$http,$window,$rootScope,$location) {	
			$rootScope.checkConnection();
			$rootScope.changeview();
    		$scope.title = 'All Projects';
    		$scope.loader = true;
			$scope.selectedcity = '1';
    		$scope.selectedstatus = '';
    		
    		$scope.imgappend = '';
    		if(window.innerWidth <= 480){
    		$scope.imgappend = "_mobile.jpg";
    		}else {
    		$scope.imgappend = "";
    		}
    		
  		$scope.projectType = null;
  		$scope.projectList = null;
    	$http.get('http://www.hindva.com/data/producttype.php')
    	 	.success(function (data) {
				$scope.projectType = data;
				$scope.selectedcity = $scope.projectType[0].typeid;
				
				$http.get('http://www.hindva.com/data/projectlist.php')
				.success(function (data) {
					$scope.projectList = data;
					$scope.loader = false;
			
				}).error(function (data, status, headers, config) {}); 
				
    	 }).error(function (data, status, headers, config) {}); 
     	
    	
     	$scope.$watchCollection('[loader]', function (newvalue, oldvalue, $scope) {	
		});
		
		$scope.status = { isopen: false };     
     
     	$scope.changetype = function(type) 
     	{	
			if(type == 'All')
			{
				$scope.selectedstatus = '';
			}
			else
			{
				$scope.selectedstatus =  type;
			}
			$scope.title = type +  ' Projects';
			$scope.status.isopen = !$scope.status.isopen;		 			
     	};	
     	
      	$scope.changecity = function(cityID) 
      	{
			$scope.selectedcity = cityID ;	
      	};	
     	$scope.backfun = function() { $location.path('/home'); } 	
     		
      	document.removeEventListener("backbutton", onBackKeyDown, false);		
});

hindvaApp.controller('projectController', function($scope,$http,$routeParams,$window,$rootScope,$location) {	
	$rootScope.checkConnection();
		$rootScope.changeview();
  		$scope.loader = true;
  		$scope.imgappend = '';
    		if(window.innerWidth <= 480){
    			$scope.imgappend = "_mobile.jpg";
    		}else {
    			$scope.imgappend = "";
    		}
    		
    			
    		$scope.projectList = null;
    		$scope.projectId = $routeParams.projectId;
    		$scope.projectType = null;
    		$scope.category =null;
    		//var temp = '';
    		
		var dataObject = {
          	typeid : $scope.projectId 
       	};
      	// serialize data before sending       
      	var serdata = serialize(dataObject);
      
		$http({
    	 	 url: "http://www.hindva.com/data/projectlist.php",
			method: "POST",
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        	data: serdata
    		}).success(function(data, status, headers, config) {
				$scope.projectList = data;
				$scope.title = $scope.projectList[0].typeparent;
				$scope.category = $scope.projectList[0].typetitle;
      		
				$http({
					url: "http://www.hindva.com/data/producttype.php",
					method: "POST",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: serdata
					}).success(function(data, status, headers, config) {
						$scope.projectType = data;
						$scope.loader = false;
					}).error(function(data, status, headers, config) {
					
				});
      		
    		}).error(function(data, status, headers, config) {
     	
		});	 
     	
		 	$scope.changecat = function(typeID) {
					$scope.category = typeID ;	
      	 };	
      	 $scope.backfun = function() {
      			$location.path('/ProjectSelect');
      	 } 	
      	 document.removeEventListener("backbutton", onBackKeyDown, false);
});

hindvaApp.controller('projectDetailController', function($scope,$http,$routeParams,$rootScope,$location,$modal) {
	$rootScope.checkConnection();
	 $scope.downloading = false ; 
	 $scope.downloadmsg = '';
  	 $rootScope.changeview();
    	 $scope.projectDetail = null;
    	 $scope.projectId = $routeParams.projectId;
    	 $scope.display = true;
    	 $scope.code = null;
    	 $scope.loader = true;
		$scope.doverview 	= true;
		$scope.dfloorplan = false;
		$scope.dlocation 	= false;
		$scope.dbrochure 	= false;
		$scope.dvideo 		= false;
		$scope.dshare		= false;
		$scope.dprojectgallery		= false;
		$scope.dsitegallery		= false;
		$scope.imgappend = '';
		if(window.innerWidth <= 480){
				$scope.imgappend = "_mobile.jpg";
		 }else {
				$scope.imgappend = "";
			}
   
		var dataObject = {
          	projectid : $scope.projectId 
       	};
      // serialize data before sending       
      	var serdata = serialize(dataObject);
		$http({
    	  url: "http://www.hindva.com/data/projectdetail.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    	   }).success(function(data, status, headers, config) {
			$scope.projectDetail = data;
			
			$scope.floorarray = $scope.projectDetail.floorplan.split(',');
			$scope.floortitlearray = $scope.projectDetail.floorplantitle.split(',');
			$scope.code = $scope.projectDetail.videourl;
      	
			if($scope.floorarray == '')
			{
				$scope.display = false;
			}
				 $scope.loader = false;
    		}).error(function(data, status, headers, config) {
     	
		});
		
		$scope.changeview = function(selectedtab) {       
     			if(selectedtab == 'overview')
     			{
     				$scope.doverview 	= true;
					$scope.dfloorplan 	= false;
					$scope.dlocation 	= false;
					$scope.dbrochure 	= false;
					$scope.dvideo 		= false;
					$scope.dshare		= false;
					$scope.dprojectgallery = false;
					$scope.dsitegallery		= false;
     			}
				else if(selectedtab == 'floorplan')  
				{
					$scope.doverview 	= false;
					$scope.dfloorplan 	= true;
					$scope.dlocation 	= false;
					$scope.dbrochure 	= false;
					$scope.dvideo 		= false;
					$scope.dshare		= false;
					$scope.dprojectgallery = false;
					$scope.dsitegallery		= false;
				} 
				else if(selectedtab == 'brochure')  
				{
					
        				navigator.notification.confirm(
            			'Do You want to download the brochure?',  
            			function(button){  
            			 if(button == 1){ window.openFileNative.open($scope.projectDetail.brochure); 
       				 }
       				 },        
            			'Brochure Download',            
            			'Yes,No'        
       				 );
       		
				}  
				else if(selectedtab == 'location')  
				{
					$scope.doverview 	= false;
					$scope.dfloorplan 	= false;
					$scope.dlocation 	= true;
					$scope.dbrochure 	= false;
					$scope.dvideo 		= false;
					$scope.dshare		= false;
					$scope.dprojectgallery = false;
					$scope.dsitegallery		= false;
				}  
				else if(selectedtab == 'projectgallery')  
				{
					$scope.doverview 	= false;
					$scope.dfloorplan 	= false;
					$scope.dlocation 	= false;
					$scope.dbrochure 	= false;
					$scope.dvideo 		= false;
					$scope.dshare		= false;
					$scope.dprojectgallery = true;
					$scope.dsitegallery		= false;
				}  
				else if(selectedtab == 'sitegallery')  
				{
					$scope.doverview 	= false;
					$scope.dfloorplan 	= false;
					$scope.dlocation 	= false;
					$scope.dbrochure 	= false;
					$scope.dvideo 		= false;
					$scope.dshare		= false;
					$scope.dprojectgallery	= false;
					$scope.dsitegallery		= true;
				} 
				else if(selectedtab == 'video')  
				{
					var videoid = $scope.projectDetail.videourl;
					cordova.plugins.videoPlayer.play('http://www.youtube.com/watch?v=' + videoid);
				
				}  
				else if(selectedtab == 'share')  
				{
					var overviewtxt= removeHTMLTags();
					var imgsrc = $scope.projectDetail.gallery + $scope.imgappend ; 
					
					window.plugins.socialsharing.share(overviewtxt,$scope.projectDetail.title, imgsrc, $scope.projectDetail.weburl);
					
				}    
       }
  	 $scope.backfun = function() {
      			$location.path('/ProjectSelect');
      } 
      
      /* modal box */
    

	$scope.animationsEnabled = true;

	$scope.openimg = function (imgname,name) {
		
			var modalInstance = $modal.open({
			  animation: $scope.animationsEnabled,
			  templateUrl: 'imgModalContent.html',
			  controller: 'ImgModalInstanceCtrl',
			  size: 'lg',
			  resolve: {
			  imgsrc: function () {
				  return imgname;
			  },
			   imgnm: function () {
				  return name;
			  }
			}
			});
		
	};
	
  /* end of modal box */
      document.removeEventListener("backbutton", onBackKeyDown, false);	
});

hindvaApp.controller('projectoptionController', function($scope,$http,$location,$rootScope,$location) {
			$rootScope.checkConnection();
			$rootScope.changeview();
  			$scope.loader = true;
			$scope.status = 'Ongoing';	  
			$scope.title = 'Search Projects';	
			
    			
	     
	      	$scope.project = '';
			$scope.projectCity = null;
			$scope.projectType = null;
			$http.get('http://www.hindva.com/data/producttype.php')
         		.success(function (data) {
             		$scope.projectCity = data;
             		$scope.project = $scope.projectCity[0];
             		$scope.projectc = $scope.projectCity[0].typeid;
             		
						$scope.typeId  = '1';
						/* 2nd dropdown*/
						 var dataObject = {
							typeid : '1'
						};
					
						$scope.type = '';
						var serdata = serialize(dataObject);
						$http({
						url: "http://www.hindva.com/data/producttype.php",
						method: "POST",
						headers: {'Content-Type': 'application/x-www-form-urlencoded'},
						data: serdata
						}).success(function(data, status, headers, config) {
							$scope.projectType = data;
							$scope.type = $scope.projectType[0];
							$scope.projectt = $scope.projectType[0].typeid;
							$scope.loader = false;
					 }).error(function(data, status, headers, config) {
     			
				});	 
     			
         		})
         		.error(function (data, status, headers, config) {
         	});
         	
         	
        		/* start of function on change */ 	
        
       	 $scope.getProjects = function(typeID) {
				 var dataObject = {
        			typeid : typeID
     			};
     			
     			var serdata = serialize(dataObject);
     			$http({
    	  		url: "http://www.hindva.com/data/producttype.php",
        		method: "POST",
        		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        		data: serdata
    			}).success(function(data, status, headers, config) {
      			$scope.projectType = data;
      			$scope.type = $scope.projectType[0];
      			$scope.projectt = $scope.projectType[0].typeid;
    			}).error(function(data, status, headers, config) {
     				
				});	 
       };
       
        $scope.submitSearch = function() {
				var selectedcity= $scope.projectc;
				var selectedtype= $scope.projectt ;
				var selectedstatus= $scope.status;
				
				$location.path('/Search-Project/'+selectedtype + '/' + selectedstatus,false);
       };
       $scope.backfun = function() {
      			$location.path('/home');
      } 	
      document.removeEventListener("backbutton", onBackKeyDown, false);
       
       /* end of function*/
         	
});
hindvaApp.controller('searchProjectController', function($scope,$http,$routeParams,$window,$rootScope,$location) {
	
	$rootScope.checkConnection();
	$rootScope.changeview();
	$scope.title = "Projects";
	$scope.loader = true;
	$scope.imgappend = '';
   	if(window.innerWidth <= 480){
    		$scope.imgappend = "_mobile.jpg";
    	}else {
    		$scope.imgappend = "";
    	}
	
 
    	$scope.projectList = null;
    	$scope.type = $routeParams.typeID;
    	$scope.status = $routeParams.status;
    	$scope.titlestatus = $routeParams.status;
    	$scope.detail = null;
	
    	if($scope.status == 'All')
    	{
    		$scope.status = '';
    	}
		  var dataObject = {
          typeid : $scope.type
       };
      // serialize data before sending       
      var serdata = serialize(dataObject);
     
	  $http({
    	  url: "http://www.hindva.com/data/searchproject.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    	}).success(function(data, status, headers, config) {
      		$scope.projectList = data;
      		
      		 var dataObject1 = {
				project : $scope.type
			};
			var serdata1 = serialize(dataObject1);
			$http({
    	  		url: "http://www.hindva.com/data/producttype.php",
        		method: "POST",
        		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        		data: serdata1
    			}).success(function(data, status, headers, config) {
						$scope.detail = data;
						$scope.loader = false;
    				
    			}).error(function(data, status, headers, config) {
     		
				});	
      		
			}).error(function(data, status, headers, config) {
     		
			});
		
		
		  	$scope.backfun = function() {
      			$location.path('/ProjectSelect');
      		}
      	document.removeEventListener("backbutton", onBackKeyDown, false);		
});
hindvaApp.controller('reachusController', function($scope,$rootScope,$location) {
	$rootScope.changeview();
	
	$scope.title = "Reach Us";
	
	$scope.items = [
                    {
                        name: "item1",
                        desc: "Surat",
                        content : "C/o M Kantilal Exports,<br>251, Shree Ambika, Near Yash Plaza,<br>Varachha Road, Surat 395006<br><i class='fa fa-phone'>&nbsp;&nbsp;</i>+91 261 6519111<br><i class='fa fa-phone'>&nbsp;&nbsp;</i>91 80000 11811 (24 Hours)<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>surat@hindva.com"
  		
                    },
                    {
                        name: "item2",
                        desc: "Ahmedabad",
                        content : "Shantiniketan Solitaire,<br>Opp. Vrundavan Party Plot,<br>S.P. Ring Road, Nikol, Ahmedabad<br><i class='fa fa-phone'>&nbsp;&nbsp;</i>+91 79 65458991<br><i class='fa fa-phone'>&nbsp;&nbsp;</i>+91 79 30119020 (24 Hours)<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>ahmedabad@hindva.com"
                        
                    },
                    {
                        name: "item3",
                        desc: "Mumbai",
                        content : "905, 9th Floor, Business Suites-9,<br>83, S.V. Road, Near Dynasty Restaurant,<br>Santacruz (West), Mumbai 400054<br><i class='fa fa-phone'>&nbsp;&nbsp;</i> +91 22 26481010<br><i class='fa fa-print'>&nbsp;&nbsp;</i>  +91 22 26481012 (Fax)<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>mumbai@hindva.com"
                        
                    },
                    {
                        name: "item4",
                        desc: "Contact Emails",
                        content : "<span><strong>Sales and Services : </strong><br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>info@hindva.com </span><br><p></p><span><strong>Business Communication : </strong><br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>response@hindva.com</span><br /><p></p><span><strong>Construction and Engineering : </strong><br> <i class='fa fa-envelope'>&nbsp;&nbsp;</i>projects@hindva.com</span><br /><p></p><span><strong>Finance : </strong><br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>finance@hindva.com</span><br />"  
                    },
              		{
                       name: "item5",
                        desc: "Social Connects",
                         content : "<a class='social-link' href='' onclick='openfacebook()'><i class='fa fa-facebook'>&nbsp;&nbsp;</i>Like Us on Facebook</a><a class='social-link' onclick='opentwitter()' href=''><i class='fa fa-twitter'>&nbsp;&nbsp;</i>Follow us on Twitter</a><a class='social-link' href='' onclick='openlinked()'><i class='fa fa-linkedin'>&nbsp;&nbsp;</i>Follow Us on LinkedIn</a><a class='social-link' onclick='openyoutube()' href=''><i class='fa fa-youtube'>&nbsp;&nbsp;</i>Follow Us on Youtube</a><p class='rate'>Love Hindva Builders ?<br>Help Us to make even better by rating the app!<br>Click on the stars to rate!<br><a href='' class='heart-red' onclick='navigateToStore()'><i class='fa fa-star'>&nbsp;&nbsp;</i><i class='fa fa-star'>&nbsp;&nbsp;</i><i class='fa fa-star'>&nbsp;&nbsp;</i><i class='fa fa-star'>&nbsp;&nbsp;</i><i class='fa fa-star'>&nbsp;&nbsp;</i></a></p><hr><p class='support'><img src='img/reach.png' alt=''><br>&copy; 2014 Hindva Builders. All Rights Reserved.<br><span  class='color-red' href='' onclick='openhindva()'>www.hindva.com</span><br></strong><i class='fa fa-envelope'>&nbsp;&nbsp;</i>info@hindva.com <br><br>App Support : Eastern Techno Solutions<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>contact@easternts.com</p>"  
                    }
                ];

			$scope.default = $scope.items[1];
			$scope.$parent.isopen = ($scope.$parent.default === $scope.item);

                $scope.$watch('isopen', function (newvalue, oldvalue, $scope) {
                    $scope.$parent.isopen = newvalue;
                });
	
 	$scope.backfun = function() 
 	{
    	$location.path('/home');
    }
    
  	document.removeEventListener("backbutton", onBackKeyDown, false);	 	  
});
